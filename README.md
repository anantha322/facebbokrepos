#  Sample
[![js-standard-style](https://img.shields.io/badge/code%20style-standard-brightgreen.svg?style=flat)](http://standardjs.com/)

* Standard compliant React Native App 

## :arrow_up: How to Setup

**Step 1:** git clone this repo:

**Step 2:** cd to the cloned repo:

**Step 3:** Install the Application with `yarn install` or `npm i`


## :arrow_forward: How to Run App

1. cd to the repo
2. Run Build for either OS
  * for iOS
    * run `react-native run-ios`
  * for Android
    * Run Genymotion
    * run `react-native run-android`

## :arrow_forward: To Do
1. Detail error handling
2. Improve type checking with Typescript. Currently I am using flow
3. Improve type checking for saga
4. Unit testing using Jest
5. CI & CD - fastlane and jenkin
6. Storybook for each component - storybook
 

