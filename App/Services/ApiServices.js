// @flow
import API from './Api'

const api = API.create()
export default (requestStr: string) => {
  return api.apiGetRequest(requestStr)
}

