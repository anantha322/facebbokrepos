// @flow
import apisauce from 'apisauce'
const create = (baseURL: string = 'https://api.github.com/') => {
    const api = apisauce.create({
        baseURL,
        headers: {
            'Cache-Control': 'no-cache'
        },
        timeout: 10000
    })
    const apiGetRequest = (endpoint: string) => api.get(endpoint)
    return {
      apiGetRequest
    }
}

// let's return back our create method as the default.
export default {
    create
}
