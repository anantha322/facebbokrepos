// @flow
import React, {Component} from 'react';
import {FlatList, View} from 'react-native';
import {compose} from 'redux'
import {connect} from 'react-redux'
import {ListItem} from 'react-native-elements'
import {SectionHeader, ErrorHandler} from './'
import type {Contributors}  from '../Types'
type Props={
  navigation: Object,
  startup: Object,
  contributors: Object
}
class ContributorList extends Component<Props> {
  loadData = (item: Contributors) => () => {
    this.props.navigation.navigate('DetailScreen',
      {
        title: item.login,
        uri: item.html_url
      })
  }
  keyExtractor = item => item.toString()

  renderListItem = (item: Contributors, index: number) => {
    return <ListItem
      roundAvatar
      key={index}
      avatar={{uri: item.avatar_url}}
      title={item.login}
      onPress={this.loadData(item)}
    />
  }

  renderItems(contributorsData) {
    return (<View>
        <SectionHeader
          headerName={'Contributors'}
          value={contributorsData.allIds.length}/>
        <FlatList
          data={contributorsData.allIds}
          renderItem={({item}) => this.renderListItem(contributorsData.byId[item], item)}
          keyExtractor={this.keyExtractor}
        /></View>
    )
  }

  renderError() {
    const {error} = this.props.contributors
    return <ErrorHandler error={error}
                         defaultMessage={'Please select Repository from menu ...'}/>
  }

  render() {
    const contributorsData = this.props.contributors.contributors
    if (contributorsData
      && contributorsData.allIds.length > 0) {
      return this.renderItems(contributorsData)
    }
    if (this.props.startup.active) {
      return this.renderError()
    }
    return null
  }

}

const mapStateToProps = state => ({
  startup: state.startup,
  contributors: state.contributors,
})

export default compose(
  connect(mapStateToProps, null),
)(ContributorList)

