// @flow
import React from 'react';
import {View, Text} from 'react-native';
import {Badge} from 'react-native-elements'
import styles from './Styles/SectionHeaderStyles'
type Props={
  headerName: string,
  value: number
}
const SectionHeader = ({headerName, value}: Props) => {
  return (
    <View style={styles.container}>
      <Text style={styles.headerText}>{headerName}</Text>
      {value ? <Badge
        value={value}
        textStyle={styles.badgeText}
      /> : null}
    </View>
  )
}
export default SectionHeader
