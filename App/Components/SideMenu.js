// @flow
import React, {Component} from 'react';
import { FlatList} from 'react-native';
import {compose} from 'redux'
import {connect} from 'react-redux'
import {ListItem} from 'react-native-elements'
import ContributorsAction from '../Redux/ContributorsRedux'
import {ErrorHandler} from './'
import RepoListsActions from '../Redux/RepoListRedux'
import getAllReposData from '../Hoc/GetAllReposData'

import type {GitRepoType}  from '../Types'
type Props={
  navigation: Object,
  startup: Object,
  repoList: Object,
  getRepoLists: Function,
  getAllContributors: Function
}
class SideMenu extends Component<Props> {
  loadSpecificRepoData = (id: number) => () => {
    this.props.navigation.closeDrawer()
    this.props.getAllContributors(id)
  }
  keyExtractor = item => item.toString()

  renderListItem = (item: GitRepoType, index: number) => {
    return <ListItem
      key={index}
      hideChevron={true}
      title={item.name}
      onPress={this.loadSpecificRepoData(item.id)}
      rightTitle={item.watchers.toString()}
    />
  }

  renderItems(repoListData) {
    return (
      <FlatList
        data={repoListData.allIds}
        renderItem={({item}) => this.renderListItem(repoListData.byId[item], item)}
        keyExtractor={this.keyExtractor}
      />
    )
  }

  renderError() {
    const {error} = this.props.repoList
    return <ErrorHandler error={error}
                         defaultMessage={'Please check network ...'} />
  }

  render() {
    const repoListData = this.props.repoList.repos
    if (repoListData
      && repoListData.allIds.length > 0) {
      return this.renderItems(repoListData)
    }
    return this.renderError()
  }

}

const mapStateToProps = state => ({
  startup: state.startup,
  repoList: state.repoList
})
const mapDispatchToProps = dispatch => ({
  getRepoLists: () => dispatch(RepoListsActions.getRepoLists()),
  getAllContributors: (id) => dispatch(ContributorsAction.getAllContributors(id))
})
export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  getAllReposData()
)(SideMenu)

