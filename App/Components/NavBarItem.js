// @flow
import React from 'react';
import {TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome'
import styles from './Styles/NavBarItemStyles'

type Props={
  iconName: string,
  onPress: Function
}
const NavBarItem =({iconName, onPress}: Props) => {
    return (
      <TouchableOpacity
        style={styles.container}
        onPress={() => onPress()}
      >
        <Icon name={iconName} size={20} color="#fff"/>
      </TouchableOpacity>
    )
}

export default NavBarItem
