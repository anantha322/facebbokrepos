import { StyleSheet } from 'react-native'
import { Colors } from '../../Themes/'

export default StyleSheet.create({
  errorMessage:{
    color: Colors.fire
  },
  defaultMessageWrapper:{
    flex:1,
    alignItems:'center',
    justifyContent:'center'
  }
})
