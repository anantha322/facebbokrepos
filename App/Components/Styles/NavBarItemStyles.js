import { StyleSheet } from 'react-native'
import { Metrics } from '../../Themes/'

export default StyleSheet.create({
  container:{
    paddingHorizontal:Metrics.doubleBaseMargin
  }
})
