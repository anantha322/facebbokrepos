import { StyleSheet } from 'react-native'
import { Metrics, Colors, Fonts } from '../../Themes/'

export default StyleSheet.create({
  container:{
    backgroundColor:Colors.facebook,
    flexDirection:'row',
    alignItems:'center',
    padding:Metrics.baseMargin
  },
  headerText:{
    flex:1,
    color:Colors.snow,
    fontWeight:'bold',
    fontSize:Fonts.size.h6
  },
  badgeText:{
    color: Colors.snow
  }
})
