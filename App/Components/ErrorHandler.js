// @flow
import React from 'react';
import {Text, View} from 'react-native';
import styles from './Styles/ErrorHandlerStyles'
import type {ErrorType}  from '../Types'

type Props={
  error: ErrorType,
  defaultMessage: string
}
const ErrorHandler =({error, defaultMessage}: Props) =>{
    if (error.errorCode) {
      return <Text style={styles.errorMessage}>{error.errorMessage}</Text>
    }
    return <View style={styles.defaultMessageWrapper}>
      <Text>{defaultMessage}</Text>
    </View>
}

export default ErrorHandler
