// @flow
import React from 'react';
import {View} from 'react-native';
import {ListItem} from 'react-native-elements'
import {connect} from "react-redux";
import {compose} from "redux";
import type {GitRepoType} from '../Types'

type Props = {
  navigation: Object,
  startup: Object,
  repoList: Object,
  selectedRepoId: number
}
const RepoDetail = ({navigation, startup, repoList, selectedRepoId}: Props) => {
  const selectedRepository: GitRepoType = repoList.repos.byId[selectedRepoId]
  const repositoryName = () => {
    return <ListItem title={'Repository Name'}
                     hideChevron={true}
                     rightTitle={selectedRepository.name}/>
  }
  const repositoryDetail = () => {
    return <ListItem onPress={() => navigation.navigate('DetailScreen', {uri: selectedRepository.html_url})}
                     title={'Repository Detail'}/>
  }
  const forkCount = () => {
    return <ListItem title={'Forks Count'}
                     hideChevron={true}
                     rightTitle={selectedRepository.forks_count.toString()}/>
  }
  const watcherCount = () => {
    return <ListItem title={'Watcher Count'}
                     hideChevron={true}
                     rightTitle={selectedRepository.watchers_count.toString()}/>
  }
  if (selectedRepoId) {
    return <View>
      {repositoryName()}
      {repositoryDetail()}
      {forkCount()}
      {watcherCount()}
    </View>
  }
  return null
}
const mapStateToProps = state => ({
  startup: state.startup,
  repoList: state.repoList,
  selectedRepoId: state.contributors.repoId
})
export default compose(
  connect(mapStateToProps, null)
)(RepoDetail)
