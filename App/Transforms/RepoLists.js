// @flow
export const normalizeState = (reposData: Array<any>) => {
  const InitialState = {byId: {}, allIds: []}
  if(reposData){
    return reposData.reduce((previous, current) => {
      return {
        byId: {...previous.byId, [current.id]: current},
        allIds: [...previous.allIds, current.id]
      }
    }, InitialState)
  }
  return InitialState
}
export const sortRepoByWatcherCount=(reposData: Array<any> )=>{
  const SortedByWatcher= Object.values(reposData).sort(function (a: any, b: any) {
    return b.watchers - a.watchers;
  })
  return normalizeState(SortedByWatcher)
}
