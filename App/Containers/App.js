// @flow
import '../Config'
import React, { Component } from 'react'
import { Provider } from 'react-redux'
import RootContainer from './RootContainer'
import createStore from '../Redux'
import { YellowBox } from 'react-native'
// create our store
const store = createStore()
YellowBox.ignoreWarnings([
  'Warning: componentWillMount is deprecated',
  'Warning: isMounted(...) is deprecated',
  'Warning: componentWillReceiveProps is deprecated',
  'Warning: componentWillUpdate is deprecated',
  'Module RCTImageLoader requires',
  'Class RCTCxxModule'
])
class App extends Component<*> {
    render() {
        return (
            <Provider store={store}>
                <RootContainer />
            </Provider>
        )
    }
}
export default App
