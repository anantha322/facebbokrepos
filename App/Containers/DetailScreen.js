// @flow
import React, { Component } from 'react'
import { WebView } from 'react-native'
import { connect } from 'react-redux'

class DetailScreen extends Component<*> {
  static navigationOptions = ({ navigation }) => {
    const { params } = navigation.state
    return {
      headerTitle: params ? params.title : 'Detail Screen'
    }
  }
  render() {
    const { uri } = this.props
    return <WebView source={{ uri: uri }} />
  }
}

const mapStateToProps = (state, props) => ({
  ...props.navigation.state.params
})

export default connect(
  mapStateToProps,
  null
)(DetailScreen)
