// @flow
import React from 'react'
import { ScrollView} from 'react-native'
import {ContributorList, RepoDetail} from '../Components'
// Styles
import styles from './Styles/LaunchScreenStyles'
type Props={
  navigation:Object
}
export default ({navigation}:Props) => {
        return (
            <ScrollView style={styles.mainContainer}>
              <RepoDetail  navigation={navigation}/>
              <ContributorList navigation={navigation}/>
            </ScrollView>
        )
}
