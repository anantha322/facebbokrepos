import {takeLatest, all} from 'redux-saga/effects'
/* ------------- Types ------------- */

import {StartupTypes} from '../Redux/StartupRedux'
import {RepoListTypes} from '../Redux/RepoListRedux'
import { ContributorsTypes } from '../Redux/ContributorsRedux'

/* ------------- Sagas ------------- */

import {startup} from './StartupSagas'
import {getRepoLists} from './RepoListSaga'
import {getAllContributors} from './ContributorsSaga'

/* ------------- Connect Types To Sagas ------------- */

export default function* root() {
  yield all([
    // some sagas only receive an action
    takeLatest(StartupTypes.STARTUP, startup),
    takeLatest(RepoListTypes.GET_REPO_LISTS, getRepoLists),
    takeLatest(ContributorsTypes.GET_ALL_CONTRIBUTORS,getAllContributors)

  ])
}
