// @flow
import { put } from 'redux-saga/effects'
import ApiServices from '../Services/ApiServices'
import RepoListAction from '../Redux/RepoListRedux'

function* errorHanding(e = null) : Generator<*,*,*>{
  // TODO : Handle error here more specific
  const error ={
    errorCode:'ERROR_REPO_LISTS',
    errorMessage:'Something Went Wrong, please check your network'
  }
  yield put(RepoListAction.errorRepoLists(error))
}
export function* getRepoLists() : Generator<*,*,*>{
    try{
      const response=yield ApiServices('orgs/facebook/repos')
      console.log(response)
      if(response.ok && response.data ){
        yield put(RepoListAction.saveRepoLists(response.data))
      } else {
        yield errorHanding()
      }
    }
    catch (e) {
      yield errorHanding(e)
    }
}
