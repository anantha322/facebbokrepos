// @flow
import { put, select } from 'redux-saga/effects'
import ApiServices from '../Services/ApiServices'
import ContributorsAction  from '../Redux/ContributorsRedux'

const getRepoItemData= (state,id) => state.repoList.repos.byId[id]

function* errorHanding(e = null) : Generator<*,*,*>{
  // TODO : Handle error here more specific
  const error ={
    errorCode:'ERROR_CONTRIBUTOR_LISTS',
    errorMessage:'Something Went Wrong, please check your network'
  }
  yield put(ContributorsAction.errorAllContributors(error))
}
export function* getAllContributors(actions:Object) : Generator<*,*,*>{
  const {id} = actions
  const selectedRepoItemData = yield select(getRepoItemData,id)

  try{
    const response=yield ApiServices(`repos/facebook/${selectedRepoItemData.name}/contributors`)
    if(response.ok && response.data){
      yield put(ContributorsAction.saveAllContributors(response.data))
    } else{
      yield errorHanding()
    }
  }
  catch (e) {
    yield errorHanding(e)
  }
}
