// @flow
import React, { Component } from 'react'
//Type checking
import type {ComponentType} from 'react'
type wrappedComponentType = ComponentType<*>
type getAllReposDataType = () => wrappedComponentType => wrappedComponentType

const getAllReposData: getAllReposDataType = () => WrappedComponent => {
  class GetAllReposData extends Component<*> {
    componentDidMount(){
      this.props.getRepoLists()
    }

    componentDidUpdate(prevProps) {
      const {startup} = this.props
      if (startup.active !== prevProps.startup.active) {
        this.props.getRepoLists()
      }
    }

    render() {
      return <WrappedComponent
        {...this.state}
        {...this.props} />
    }
  }

  return GetAllReposData
}
export default getAllReposData
