// @flow

import { createActions, createReducer } from 'reduxsauce'
import { normalizeState } from '../Transforms/RepoLists'
import type {ErrorType, Contributors} from '../Types'
const { Types, Creators } = createActions({
  getAllContributors: ['id'],
  saveAllContributors: ['contributorsList'],
  errorAllContributors:['error']
})

export const ContributorsTypes = Types
export default Creators
type State = {
  repoId: number | null,
  contributors: {
    allIds: Array<string>,
    byId: Object
  },
  error:ErrorType
}

export const INITIAL_STATE: State = {
  repoId: null,
  contributors: {
    allIds: [],
    byId: {}
  },
  error: {
    errorCode: '',
    errorMessage: ''
  }
}

export const saveAllContributors = (state: State, { contributorsList }:{contributorsList:Array<Contributors>}) => ({
  ...state,
  contributors: normalizeState(contributorsList),
  error: INITIAL_STATE.error
})
export const getAllContributors = (state: State, { id }:{id:number}) => ({
  ...state,
  repoId: id,
  error: INITIAL_STATE.error
})
export const errorAllContributors = (state: State, { error }:{error:ErrorType}) =>
  ({
    ...INITIAL_STATE,
    error: error
  }: State)


export const reducer = createReducer(INITIAL_STATE, {
  [Types.GET_ALL_CONTRIBUTORS]: getAllContributors,
  [Types.SAVE_ALL_CONTRIBUTORS]: saveAllContributors,
  [Types.ERROR_ALL_CONTRIBUTORS]: errorAllContributors
})
