// @flow

import { createActions, createReducer } from 'reduxsauce'
import { sortRepoByWatcherCount } from '../Transforms/RepoLists'
import type {ErrorType, GitRepoType} from "../Types";

const { Types, Creators } = createActions({
  getRepoLists: null,
  saveRepoLists: ['repoLists'],
  errorRepoLists:['error']
})

export const RepoListTypes = Types
export default Creators
type State = {
  repos: {
    allIds: Array<string>,
    byId: Object
  },
  error:ErrorType
}

export const INITIAL_STATE: State = {
  repos: {
    allIds: [],
    byId: {}
  },
  error: {
    errorCode: '',
    errorMessage: ''
  }
}

export const saveRepoLists = (state: State, { repoLists }:{repoLists:Array<GitRepoType>}) => ({
  ...state,
  repos: sortRepoByWatcherCount(repoLists),
  error: INITIAL_STATE.error
})
export const errorRepoLists = (state: State, { error }:{error:ErrorType}) =>
  ({
    ...INITIAL_STATE,
    error: error
  }: State)


export const reducer = createReducer(INITIAL_STATE, {
  [Types.GET_REPO_LISTS]: null,
  [Types.SAVE_REPO_LISTS]: saveRepoLists,
  [Types.ERROR_REPO_LISTS]: errorRepoLists
})
