//import all your reducers here
export const startup = require('./StartupRedux').reducer
export const repoList = require('./RepoListRedux').reducer
export const contributors = require('./ContributorsRedux').reducer

