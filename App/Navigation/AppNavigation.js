import React from 'react'
import { DrawerNavigator, StackNavigator } from 'react-navigation'
import LaunchScreen from '../Containers/LaunchScreen'
import DetailScreen from '../Containers/DetailScreen'
import Icon from 'react-native-vector-icons/FontAwesome';
import {Colors} from '../Themes'
import styles from './Styles/NavigationStyles'
import {NavBarItem, SideMenu} from '../Components'
import {
  getDrawerConfig,
  getDrawerNavigationOptions,
  getNavigationOptionsWithAction,
  getNavigationOptions
} from '../Lib/Helpers'
const getDrawerItem = navigation => (
  <NavBarItem
    iconName="bars"
    onPress={() => {
      if (navigation.state.index === 0) {
        navigation.openDrawer()
      } else {
        navigation.closeDrawer()
      }
    }}
  />
)
const getDrawerIcon = (iconName, tintColor) => <Icon name={iconName} size={20} color={tintColor} />;

const homeDrawerIcon = ({ tintColor }) => getDrawerIcon('home', tintColor);

const homeNavOptions = getDrawerNavigationOptions('Home', Colors.primary, 'white', homeDrawerIcon);

const Drawer = DrawerNavigator({
  HomeScreen: { screen: LaunchScreen, navigationOptions: homeNavOptions },
}, getDrawerConfig(300, 'left', SideMenu));

Drawer.navigationOptions = ({ navigation }) => getNavigationOptionsWithAction('Repository', Colors.facebook, 'white', getDrawerItem(navigation));

const createStackNavigator = StackNavigator({
  Drawer: { screen: Drawer },
  DetailScreen:{ screen: DetailScreen, navigationOptions:getNavigationOptions('Repository Detail', Colors.facebook, 'white')}
}, {
  headerStyle:styles.header
});
export default createStackNavigator
